# Change Log

All notable changes to this project will be documented in this file.
See [Conventional Commits](https://conventionalcommits.org) for commit guidelines.

## 2.0.1 (2020-10-24)

**Note:** Version bump only for package generator-node-puzzle-react
